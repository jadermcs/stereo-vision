# Stereo Vision

# Compiling

mkdir build
cd build
cmake ..
make

# Usage

Estimates depth based on window matching and camera distance.

Usage:
  ./build/main [params] leftimage rightimage calibration groundtruth

        -?, -h, --help, --usage (value:true)
                shows this message
        --bad, --badthreshold, --bt (value:2.0)
                bad percentage metric threshold
        --semiglobal, --sg (value:false)
                use semi-global matching algorithm instead of block matching
        --windowsize, --ws (value:7)
                window size for template matching

        leftimage (value:data/Middlebury/Motorcycle-perfect/im0.png)
                left image path
        rightimage (value:data/Middlebury/Motorcycle-perfect/im1.png)
                right image path
        calibration (value:data/Middlebury/Motorcycle-perfect/calib.txt)
                calibration file path
        groundtruth (value:data/Middlebury/Motorcycle-perfect/disp0-n.pgm)
                disparity ground truth image path

To view the estimated depth distance in milimeters, use the left mouse button and click anywhere on the windows. The measures will be outputed to the terminal.
