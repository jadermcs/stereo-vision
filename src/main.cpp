#include <opencv2/opencv.hpp>
#include <cstdio>
#include <fstream>
#include "window_matching.h"
#include "rectify.h"

void checkIfFileExists(const cv::String& path);

int main(int argc, char** argv)
{
    setvbuf(stdout, nullptr, _IONBF, 0);
    std::vector<std::string> args(argv + 1, argv + argc);

    cv::CommandLineParser commandLineParser(argc, argv,
       "{help h usage ? | | shows this message}"

       "{@leftimage | data/Middlebury/Motorcycle-perfect/im0.png | left image path }"
       "{@rightimage | data/Middlebury/Motorcycle-perfect/im1.png | right image path }"
       "{@calibration | data/Middlebury/Motorcycle-perfect/calib.txt | calibration file path }"
       "{@groundtruth | data/Middlebury/Motorcycle-perfect/disp0-n.pgm | disparity ground truth image path }"

//       "{@leftimage | data/Middlebury/Jadeplant-perfect/im0.png | left image path }"
//       "{@rightimage | data/Middlebury/Jadeplant-perfect/im1.png | right image path }"
//       "{@calibration | data/Middlebury/Jadeplant-perfect/calib.txt | calibration file path }"
//       "{@groundtruth | data/Middlebury/Jadeplant-perfect/disp0-n.pgm | disparity ground truth image path }"

       "{windowsize ws | 7 | window size for template matching }"
       "{bad badthreshold bt | 2.0 | bad percentage metric threshold }"
       "{semiglobal sg | false | use semi-global matching algorithm instead of block matching }"
    );

    if (!commandLineParser.check())
    {
        commandLineParser.printErrors();
        return 0;
    }

    if (commandLineParser.has("help"))
    {
        commandLineParser.about(
            "Stereo Vision\n\n"
            "Estimates depth based on window matching and camera distance.\n"
        );
        commandLineParser.printMessage();
        return 0;
    }

    cv::String pathLeftImage = commandLineParser.get<cv::String>("@leftimage");
    cv::String pathRightImage = commandLineParser.get<cv::String>("@rightimage");
    cv::String pathCalibration = commandLineParser.get<cv::String>("@calibration");

    checkIfFileExists(pathLeftImage);
    checkIfFileExists(pathRightImage);
    checkIfFileExists(pathCalibration);

    WindowMatching wm(
        pathLeftImage,
        pathRightImage,
        pathCalibration
    );

    cv::Mat groundTruth = cv::imread(commandLineParser.get<cv::String>("@groundtruth"));

    if (groundTruth.dims != 1)
        cv::cvtColor(groundTruth, groundTruth, cv::COLOR_BGR2GRAY, 1);

    if (groundTruth.type() != CV_32F)
        groundTruth.convertTo(groundTruth, CV_32F);

    wm.setWindowSize(commandLineParser.get<int32_t>("windowsize"));
    wm.calculateDisparity(commandLineParser.get<bool>("semiglobal"));
    wm.calculateDepth();
    wm.saveOutputs();
    wm.printMetrics(groundTruth, commandLineParser.get<float>("badthreshold"));
    wm.showStereo();

    while (true)
    {
        int32_t key = cv::waitKeyEx(30);

        if (key == 'q' || key == 27)
            break;
    }

    cv::destroyAllWindows();
    return 0;
}

void checkIfFileExists(const cv::String& path)
{
    std::fstream check(path, std::ios_base::in);

    if (!check.is_open())
    {
        printf("Unable to open file \"%s\".\n", path.c_str());
        std::exit(0);
    }
}
