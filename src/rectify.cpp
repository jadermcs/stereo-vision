#include "rectify.h"

Rectify::Rectify(const cv::String& leftCalibrationPath,
                 const cv::String& rightCalibrationPath)
{
    cv::stereoRectify(camMatrixLeft, cv::noArray(),
                      camMatrixRight, cv::noArray(),
                      imageSize, R, T, R1, R2, P1, P2, Q, 0);

    isVerticalStereo = fabs(P2.at<double>(1, 3)) > fabs(P2.at<double>(0, 3));

    cv::Mat left1, left2, right1, right2, imgL, imgR;
    cv::initUndistortRectifyMap(camMatrixRight, cv::noArray(), R1, P1,
                                imageSize, CV_16SC2, left1, left2);
    cv::initUndistortRectifyMap(camMatrixRight, cv::noArray(), R2, P2,
                                imageSize, CV_16SC2, right1, right2);

}
