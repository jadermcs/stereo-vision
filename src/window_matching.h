#ifndef WINDOW_MATCHING_H
#define WINDOW_MATCHING_H

#include <opencv2/opencv.hpp>
#include <opencv2/calib3d.hpp>
#include <opencv2/imgproc.hpp>
#include <opencv2/highgui.hpp>
typedef cv::Point3_<uint8_t> Pixel;

class WindowMatching
{
public:

    struct BadPercentageResult
    {
        uint32_t samplesConsidered = 0;
        uint32_t badCount = 0;
        uint32_t invalidCount = 0;
        double badPercent = 0.;
        double invalidPercent = 0.;
        double averageError = 0.;
    };

    WindowMatching(const cv::String& leftImagePath, const cv::String& rightImagePath, const cv::String& calibrationFilePath);
    void setWindowSize(int32_t ws) { m_windowSize = ws; }
    void calculateDisparity(bool useSemiglobalMatchingAlgorithm = false);
    void calculateDepth();
    void saveOutputs();
    void showStereo();

    BadPercentageResult getBadPercentage(cv::Mat groundTruth, cv::Mat disparity, float badThreshold);
    void printMetrics(cv::Mat groundTruth, float badThreshold);

private:
    int32_t m_windowSize = 23;

    int32_t m_numberOfDisparities;
    double m_dOffs;
    double m_baseline;
    double m_fValue;

    cv::Mat m_left;
    cv::Mat m_leftGray;
    cv::Mat m_right;
    cv::Mat m_rightGray;

    cv::Mat m_disparity;
    cv::Mat m_disparityRaw;
    cv::Mat m_disparityNormalized;
    cv::Mat m_disparityColor;

    cv::Mat m_depth;
    cv::Mat m_depthNormalized;
    cv::Mat m_depthColor;
};


#endif /* WINDOW_MATCHING_H */
