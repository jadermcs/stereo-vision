#include "window_matching.h"
#include "mouse_events.h"
#include <fstream>
#include <string>
#include <streambuf>
#include <regex>
#include <cstdlib>
#include <cstdio>

#define SHOW_WINDOW(name, image) \
    cv::namedWindow(name, 0); \
    cv::resizeWindow(name, 800, 600); \
    cv::imshow(name, image);

static void getMinMaxNoInfinity(cv::Mat input, double& minV, double& maxV)
{
    if (input.type() != CV_32F)
        return;

    minV = INFINITY;
    maxV = -INFINITY;

    for (int32_t y = 0; y < input.rows; y++)
    {
        for (int32_t x = 0; x < input.cols; x++)
        {
            double value = static_cast<double>(input.at<float>(y, x));

            if (value != INFINITY)
            {
                minV = std::min(minV, value);
                maxV = std::max(maxV, value);
            }
        }
    }
}

WindowMatching::WindowMatching(const cv::String& leftImagePath, const cv::String& rightImagePath, const cv::String& calibrationFilePath)
{
    std::ifstream file(calibrationFilePath);
    std::string fileData;

    file.seekg(0, std::ios::end);
    fileData.reserve(file.tellg());
    file.seekg(0, std::ios::beg);
    fileData.assign((std::istreambuf_iterator<char>(file)), std::istreambuf_iterator<char>());

    std::regex fieldMatcher("(.*?)=([^\n]+)");
    std::smatch matches;

    m_numberOfDisparities = 160;

    while (std::regex_search(fileData, matches, fieldMatcher))
    {
        if (matches[1] == "ndisp")
        {
            m_numberOfDisparities = std::atoi(matches[2].str().c_str());
        }
        else if (matches[1] == "doffs")
        {
            m_dOffs = std::atof(matches[2].str().c_str());
        }
        else if (matches[1] == "baseline")
        {
            m_baseline = std::atof(matches[2].str().c_str());
        }
        else if (matches[1] == "cam0")
        {
            std::smatch focalMatch;
            std::string cameraValues = matches[2].str();
            std::regex_search(cameraValues, focalMatch, std::regex("\\[([0-9.]+)"));
            m_fValue = std::atof(focalMatch[1].str().c_str());
        }

        fileData = matches.suffix().str();
    }

    printf("Params read from calib:\n");
    printf(" - focal point: %g\n", m_fValue);
    printf(" - baseline: %g\n", m_baseline);
    printf(" - ndisp: %d\n", m_numberOfDisparities);
    printf(" - doffs: %g\n", m_dOffs);

    double closest16Multiple = std::ceil(static_cast<double>(m_numberOfDisparities) / 16.0) * 16.0;
    m_numberOfDisparities = static_cast<int32_t>(closest16Multiple);

    m_left = cv::imread(leftImagePath);
    m_right = cv::imread(rightImagePath);
    cv::cvtColor(m_left, m_leftGray, cv::COLOR_BGR2GRAY);
    cv::cvtColor(m_right, m_rightGray, cv::COLOR_BGR2GRAY);
}

void WindowMatching::calculateDisparity(bool useSemiglobalMatchingAlgorithm)
{
    cv::Mat disparityFixedPoint;

    if (useSemiglobalMatchingAlgorithm)
    {
        cv::Ptr<cv::StereoSGBM> sbm = cv::StereoSGBM::create(0, m_numberOfDisparities, m_windowSize);
        sbm->compute(m_leftGray, m_rightGray, disparityFixedPoint);
    }
    else
    {
        cv::Ptr<cv::StereoBM> sbm = cv::StereoBM::create(m_numberOfDisparities, m_windowSize);
        sbm->compute(m_leftGray, m_rightGray, disparityFixedPoint);
    }

    m_disparityRaw = cv::Mat(disparityFixedPoint.rows, disparityFixedPoint.cols, CV_32FC1);
    m_disparityRaw.forEach<float>([&] (float& value, const int position[]) -> void {
        uint16_t rawValue = disparityFixedPoint.at<uint16_t>(position[0], position[1]);
        if (rawValue == 0xFFF0)
            value = 0.f;
        else
            value = (rawValue >> 4) + static_cast<float>(rawValue & 0xF) / 16.0;
    });

    double minV;
    double maxV;

    getMinMaxNoInfinity(m_disparityRaw, minV, maxV);

    printf("Disparity min/max: %g/%g\n", minV, maxV);

    m_disparity = m_disparityRaw / static_cast<float>(m_disparityRaw.cols);

    m_disparityNormalized = m_disparityRaw.clone();
    m_disparityNormalized -= minV;
    m_disparityNormalized *= 254.0 / (maxV - minV);
    m_disparityNormalized.convertTo(m_disparityNormalized, CV_8UC1);

    cv::applyColorMap(m_disparityNormalized, m_disparityColor, cv::COLORMAP_JET);
}

void WindowMatching::calculateDepth()
{
    m_depth = m_disparityRaw.clone();
    m_depth.forEach<float>([&] (float& value, const int position[]) -> void {
        if (value > 0.)
            value = m_baseline * m_fValue / (value + m_dOffs);
        else
            value = INFINITY;
    });

    double minV;
    double maxV;

    getMinMaxNoInfinity(m_depth, minV, maxV);

    printf("Depth min/max: %g/%g\n", minV, maxV);

    m_depthNormalized = m_depth.clone();
    m_depthNormalized -= minV;
    m_depthNormalized *= 254.0 / (maxV - minV);

    m_depthNormalized.forEach<float>([] (float& value, const int position[]) -> void {
        if (value == INFINITY)
            value = 255.f;
    });

    m_depthNormalized.convertTo(m_depthNormalized, CV_8UC1);

    m_depthColor = 255 - m_depthNormalized;
    cv::applyColorMap(m_depthColor, m_depthColor, cv::COLORMAP_JET);
}

void WindowMatching::saveOutputs()
{
    cv::Mat disparityImage;
    m_disparity.convertTo(disparityImage, CV_8U, 255.);

    cv::imwrite("./disparidade.pgm", disparityImage);
    cv::imwrite("./profundidade.png", m_depthNormalized);
}

void WindowMatching::showStereo()
{
    static const cv::String windowDisparityMap = "Disparity Map";
    static const cv::String windowDisparityMapNorm = "Disparity Map Normalized";
    static const cv::String windowDisparityColorMap = "Disparity Map Normalized Colored";
    static const cv::String windowDepthMap = "Depth Map Normalized";
    static const cv::String windowDepthColorMap = "Depth Map Normalized Colored";

    static MouseEvents mouseEvents([&] (const cv::String& windowName, int event, cv::Vec2i position) -> void {
        if (event == cv::EVENT_LBUTTONDOWN)
        {
            printf("Clicked at %d/%d, disparity = %g, depth = %u = %g mm\n",
               position.val[0], position.val[1],
               m_disparityRaw.at<float>(position.val[1], position.val[0]),
               m_depthNormalized.at<uint8_t>(position.val[1], position.val[0]),
               m_depth.at<float>(position.val[1], position.val[0])
            );
        }
    });

    SHOW_WINDOW(windowDisparityMap, m_disparity)
    SHOW_WINDOW(windowDisparityMapNorm, m_disparityNormalized)
    SHOW_WINDOW(windowDisparityColorMap, m_disparityColor)
    SHOW_WINDOW(windowDepthMap, m_depthNormalized)
    SHOW_WINDOW(windowDepthColorMap, m_depthColor)

    mouseEvents.registerToWindow(windowDisparityMap);
    mouseEvents.registerToWindow(windowDisparityMapNorm);
    mouseEvents.registerToWindow(windowDisparityColorMap);
    mouseEvents.registerToWindow(windowDepthMap);
    mouseEvents.registerToWindow(windowDepthColorMap);
}

WindowMatching::BadPercentageResult WindowMatching::getBadPercentage(cv::Mat groundTruth, cv::Mat disparity, float badThreshold)
{
    WindowMatching::BadPercentageResult result;

    if (groundTruth.type() != CV_32F || disparity.type() != CV_32F ||
        groundTruth.cols != disparity.cols ||
        groundTruth.rows != disparity.rows)
    {
        return result;
    }

    for (int32_t y = 0; y < groundTruth.rows; y++)
    {
        for (int32_t x = 0; x < groundTruth.cols; x++)
        {
            float groundTruthDisparity = groundTruth.at<float>(y, x);

            if (groundTruthDisparity == 0.)
                continue;

            float disparityCompare = disparity.at<float>(y, x);

            result.samplesConsidered++;

            if (disparityCompare == 0.)
            {
                result.invalidCount++;
                continue;
            }

            float diff = std::abs(disparityCompare - groundTruthDisparity);

            result.averageError += static_cast<double>(diff);

            if (diff > badThreshold)
                result.badCount++;
        }
    }

    result.badPercent = 100.0 * static_cast<double>(result.badCount) / static_cast<double>(result.samplesConsidered);
    result.invalidPercent = 100.0 *  static_cast<double>(result.invalidCount) / static_cast<double>(result.samplesConsidered);
    result.averageError = result.averageError / static_cast<double>(result.samplesConsidered - result.invalidCount);

    return result;
}

void WindowMatching::printMetrics(cv::Mat groundTruth, float badThreshold)
{
    BadPercentageResult result = getBadPercentage(groundTruth, m_disparity, badThreshold);

    printf("Pixel count | Bad%.1f | Invalid | Total Bad | Avg Error\n", badThreshold);
    printf("%9u   | %5.1f%% | %5.1f%%  | %6.1f%%   | %7.1f\n",
        result.samplesConsidered, result.badPercent,
        result.invalidPercent, result.badPercent + result.invalidPercent,
        result.averageError
    );
}
