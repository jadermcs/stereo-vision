#include "mouse_events.h"
#include <functional>

static void onMouseEvent(int event, int x, int y, int flags, void* userdata)
{
    std::shared_ptr<MouseEvents::WindowUserdata> state = *static_cast<std::shared_ptr<MouseEvents::WindowUserdata>*>(userdata);
    state->position = cv::Vec2i(x, y);
    state->instance->call(state->windowName, event, state->position);
}

MouseEvents::MouseEvents(MouseCallback callback) :
    m_callback(callback) {}

void MouseEvents::registerToWindow(const cv::String& windowName)
{
    std::shared_ptr<WindowUserdata> userdata = std::make_shared<WindowUserdata>();

    userdata->instance = this;
    userdata->windowName = windowName;

    m_states[windowName] = userdata;
    cv::setMouseCallback(windowName, onMouseEvent, &m_states[windowName]);
}

void MouseEvents::call(const cv::String& windowName, int event, cv::Vec2i pos)
{
    m_callback(windowName, event, pos);
}
