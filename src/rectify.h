#ifndef RECTIFY_H
#define RECTIFY_H

#include <opencv2/core.hpp>
#include <opencv2/calib3d.hpp>
#include <opencv2/imgproc.hpp>

class Rectify
{
private:
    cv::Mat camMatrixLeft, camMatrixRight, R, T;
    cv::Size imageSize;
    // result variables
    cv::Mat R1, R2, P1, P2, Q;
    bool isVerticalStereo = false;
public:
    Rectify(const cv::String&, const cv::String&);
};

#endif /* RECTIFY_H */
