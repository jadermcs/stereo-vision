#ifndef MOUSE_EVENTS_H
#define MOUSE_EVENTS_H

#include <opencv2/opencv.hpp>
#include <map>
#include <functional>
#include <memory>

class MouseEvents
{
public:

    struct WindowUserdata
    {
        cv::String windowName;
        MouseEvents* instance;
        cv::Vec2i position;
    };

    typedef std::function<void(const cv::String&, int32_t event, cv::Vec2i)> MouseCallback;

    MouseEvents(MouseCallback callback);

    void registerToWindow(const cv::String& windowName);
    void call(const cv::String& windowName, int event, cv::Vec2i pos);

private:

    std::map<cv::String, std::shared_ptr<WindowUserdata>> m_states;
    MouseCallback m_callback;
};

#endif // MOUSE_EVENTS_H
